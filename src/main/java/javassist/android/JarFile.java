package javassist.android;

public class JarFile {
	private static final String RESOURCE_CLASSES_DEX = "classes.dex";
	
	private final ClassLoader classLoader;

	public JarFile(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}
	
	public Class<?> getClass(String classname) throws ClassNotFoundException {
		if (null == this.classLoader) {
			throw new ClassNotFoundException();
		}
		return this.classLoader.loadClass(classname);
	}
}
